define(["external/phaser.min"], function() {

      function create(sprite)
      {
          var target = new Phaser.Point(sprite.x, sprite.y);
          var drop = null;
          var shieldTime = 0;
          var dashTime = 0;
          var dashCoolDown = 0;
          var dashDir;

          var stats = {
            shielded : false,
            loaded : false,
            oxygen : 100.0,
            battery : 100.0
          };

        var power = {
          shock: false,
          dash: false,
          shield: false
        };

          var actions = Object.freeze(
            {
              move : 'move',
              pick : 'pick',
              drop : 'drop',
              shock: 'shock',
              dash: 'dash'
            });


          function act(action, param)
            {
              switch (action)
              {
                case actions.move:
                  target.x = param.target.x - sprite.width * 0.5;
                  target.y = param.target.y - sprite.height;
                  return true;

                case actions.pick:
                  if (drop)
                    return false;
                  drop = param.drop;
                  power[drop] = true;
                  return true;

                case actions.drop:
                  if (!drop)
                    return false;
                  drop = null;
                  return true;

                case actions.shock:
                  if (!power[actions.shock] || stats.battery < 100)
                    return false;
                  stats.battery = 0;
                  stats.shielded = true;
                  shieldTime = 3.0;
                  return true;

                case actions.dash:
                  if (!power[actions.dash] || dashCoolDown > 0)
                    return false;
                  stats.oxygen -= 5.0;
                  dashCoolDown = 5.0;
                  dashTime = 0.5;
                  dashDir = new Phaser.Point(target.x - sprite.x, target.y - sprite.y);
                  return true;
              }

              return false;
            }

           function update(deltaTime)
            {

              stats.loaded = (drop != null);
              stats.oxygen -= (100 / 20) * (deltaTime / 60);
              if (stats.oxygen < 0)
                stats.oxygen = 0;

              stats.battery += (100 / 3) * (deltaTime / 60);
              if (stats.battery > 100)
                stats.battery = 100;

              if (shieldTime > 0)
                shieldTime -= deltaTime;
              stats.shielded = (shieldTime > 0);


              if (dashTime > 0)
                dashTime -= deltaTime;
              if (dashCoolDown > 0)
                dashCoolDown -= deltaTime;


              var dir = new Phaser.Point(target.x - sprite.x, target.y - sprite.y);
              var dash = (dashTime > 0);

               if (dash)
               {
                 dir = dashDir;
                 target.x = sprite.body.x;
                 target.y = sprite.body.y;
               }

              sprite.body.drag.x = 0;
              sprite.body.drag.y = 0;
              if (dir.getMagnitude() > 10)
              {
                var delta = dir;

                var velocity = 200;

                if (drop)
                  velocity *= 0.9;

                if (dash)
                  velocity *= 5.0;

                if (dash || dir.getMagnitude() > 300 * deltaTime)
                  delta.setMagnitude(velocity);

                sprite.body.velocity.x = delta.x * sprite.scale.x;
                sprite.body.velocity.y = delta.y * sprite.scale.y;



                if (delta.x > 0)
                  sprite.animations.play('right');
                else
                  sprite.animations.play('left');
              }
              else
              {
                sprite.body.velocity.x = 0;
                sprite.body.velocity.y = 0;
                sprite.animations.play('stand');
              }
            }


          var instance = {
            stats : stats,
            power : power,

            sprite : sprite,

            target : target,

            actions : actions,

            act : act,

            update : update
          }

          return instance;

        }

      return {
        create: create
      }
    }
);
