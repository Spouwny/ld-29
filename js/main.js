
requirejs(["external/phaser.min", "player"], Main);


var playerFactory;

var game;
var player;
var worm;
var obstacles;
var platforms;
var drops;
var hud;
var playerShield;

var hudOxygen;
var hudBattery;
var hintText;
var shockKey;

var scale = 0.8;
//var scale = 0.03;

var emitter;
var emitterPlayer;
var emitterWind;
var emitterVessel;

function preload()
{
  game.preloadBar = game.add.sprite(356, 370, 'preloaderBar');

  game.load.setPreloadSprite(game.preloadBar);

  game.stage.disableVisibilityChange = true;

  game.load.image('desert', 'assets/sand.png');
  game.load.image('platform', 'assets/rocks.png');
  game.load.image('base', 'assets/vessel.png');
  game.load.image('base_indicator', 'assets/vessel_indicator.png');
  game.load.image('playerShield', 'assets/shield.png');
  game.load.image('shock', 'assets/box_shock.png');
  game.load.image('dash', 'assets/box_dash.png');
  game.load.image('shield', 'assets/box_shield.png');
  game.load.spritesheet('dude', 'assets/dude.png', 32, 48);
  game.load.image('worm', 'assets/smoke.png');



  game.load.image('smoke', 'assets/smoke.png');
  game.load.image('smokeBlack', 'assets/smokeBlack.png');




}


function rotate_point(point, origin, angle)
{
    return new Phaser.Point(
        Math.cos(angle) * (point.x - origin.x) - Math.sin(angle) * (point.y - origin.y) + origin.x,
        Math.sin(angle) * (point.x - origin.x) + Math.cos(angle) * (point.y - origin.y) + origin.y
    );
}

function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

function genRandomPlatform(startingPoint)
{
  var list = []
  var minX = game.world.x;
  var maxX = game.world.width;
  var minY = game.world.y;
  var maxY = game.world.height;


  var numPath = 3;

  var minVisibleDist = game.height * scale;
  if (minVisibleDist > game.width * scale)
    minVisibleDist = game.width * scale;

  var easyDist = {min: minVisibleDist * 0.75, max: minVisibleDist * 1.2};
  var mediumDist = {min: minVisibleDist * 1.2, max: minVisibleDist * 1.75};
  var hardDist = {min: minVisibleDist * 1.75, max: minVisibleDist * 2.5};

  var trainingZone = minVisibleDist * 1.5;
  var easyZone = minVisibleDist * 3.0;
  var mediumZone = minVisibleDist * 8.0;
  var hardZone = minVisibleDist * 12.0;
  var voidZone = minVisibleDist * 16.0;
  var endZone = minVisibleDist * 20.0;


  var zones = [
    {
      limit: trainingZone,
      dist: easyDist,
      bonus: [],
      unique: 0
    },
    {
      limit: easyZone,
      dist: easyDist,
      bonus: [],
      unique: 1
    },
    {
      limit: mediumZone,
      dist: mediumDist,
      bonus: [],
      unique: 1
    },
    {
      limit: hardZone,
      dist: hardDist,
      bonus: [],
      unique: 1
    },
    {
      limit: voidZone,
      dist: hardDist,
      bonus: [],
      unique: 0
    },
    ];


  for (var path = 0; path < numPath; path++)
  {
    var uniqueDrop = false;
    var angle = Phaser.Math.degToRad(path * (360 / numPath) + (180 + game.rnd.angle()) / (numPath + 1));
    var dist = game.rnd.integerInRange(easyDist.min, easyDist.max);
    var lastPoint = new Phaser.Point(startingPoint.x, startingPoint.y);
    lastPoint.setMagnitude(lastPoint.getMagnitude() + dist);
    lastPoint = rotate_point(lastPoint, startingPoint, angle)
    list.push(lastPoint);

    var lastZone = zones[0];
    while (1)
    {
      var dist = Phaser.Point.distance(startingPoint, lastPoint);

      if (dist >= endZone)
        break ;

      var zone = zones[0];
      for (var index in zones)
        if (zones[index].limit <= dist)
          zone = zones[index];

      if (!uniqueDrop && zone != lastZone)
        if (lastZone.unique)
        {
          lastPoint.dropUnique = true;
          uniqueDrop = true;
          lastZone.unique = 0;
        }

      var randAngle = Phaser.Math.degToRad(game.rnd.angle() * 0.4);
      dist = game.rnd.integerInRange(zone.dist.min, zone.dist.max);

      var nextPoint = new Phaser.Point(lastPoint.x - startingPoint.x, lastPoint.y - startingPoint.y);
      nextPoint.setMagnitude(nextPoint.getMagnitude() + dist);
      nextPoint.x += startingPoint.x;
      nextPoint.y += startingPoint.y;
      nextPoint = rotate_point(nextPoint, lastPoint, randAngle)

      list.push(nextPoint);

      lastPoint = nextPoint;
      lastZone = zone;
    }
  }

/*
  for (var i = 0; i < 100; i++)
  {
    list.push(new Phaser.Point(game.rnd.integerInRange(minX, maxX), game.rnd.integerInRange(minY, maxY)));
  }
  */
  return list;
}

function create()
{

  shockKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  shockKey.onDown.add(shock, this);

  game.physics.startSystem(Phaser.Physics.ARCADE);

  game.world.setBounds(0, 0, 800 * 1000 * scale, 600 * 1000 * scale);


  var startingPoint = new Phaser.Point(
    game.world.x + game.world.width * 0.5,
    game.world.y + game.world.height * 0.5
  );

  worm = game.add.sprite(startingPoint.x, startingPoint.y + 200, 'worm');
  game.add.tileSprite(game.world.x, game.world.y, game.world.width, game.world.height, 'desert');



  platforms = game.add.group();
  platforms.enableBody = true;

  drops = game.add.group();
  drops.enableBody = true;



  emitterWind = game.add.emitter(startingPoint.x, startingPoint.y, 200);

  emitterWind.makeParticles('smoke');
  emitterWind.gravity = 0;

  emitterWind.setXSpeed(100, 100);
  emitterWind.setYSpeed(0, 0);

  emitterWind.height  = 600;
  emitterWind.width   = 800;

  emitterWind.minParticleScale = 0.5;
  emitterWind.maxParticleScale = 0.7;

  emitterWind.minParticleAlpha = 0.1;
  emitterWind.maxParticleAlpha = 0.1;



  emitterWind.minRotation = 0;
  emitterWind.maxRotation = 200;

  emitterWind.particleSendToBack = false;


  emitterWind.start(false, 2000, 500);


  obstacles = game.add.group();
  obstacles.enableBody = true;

  hud = game.add.group();
  hud.fixedToCamera = true;


  var list = genRandomPlatform(startingPoint);
  var dropItems = ['shock', 'dash', 'shield'];

  dropItems = shuffle(dropItems);

  for (var index in list)
  {
    var platform = platforms.create(list[index].x, list[index].y, 'platform');
    platform.scale.x *= 1 * scale;
    platform.scale.y *= 1 * scale;
    platform.x = platform.x;
    platform.y = platform.y;
    platform.body.immovable = true;
    platform.anchor.setTo(0.5, 0.5);
    platform.rotation = game.rnd.angle();

    if (list[index].dropUnique)
    {
      var type = dropItems.pop();

      var drop = drops.create(list[index].x, list[index].y, type);
      drop.scale.x *= 1 * scale;
      drop.scale.y *= 1 * scale;
      drop.x = drop.x - drop.width * 0.5;
      drop.y = drop.y - drop.height * 0.5;
      drop.body.immovable = true;

      var indicator = hud.create(400, 300, type);
      indicator.drop = drop;
    }
  }




  var platform = platforms.create(startingPoint.x, startingPoint.y, 'platform');
  platform.scale.x *= 2 * scale;
  platform.scale.y *= 2 * scale;
  platform.x = platform.x - platform.width * 0.5;
  platform.y = platform.y - platform.height * 0.5;
  platform.body.immovable = true;






  var base = obstacles.create(startingPoint.x, startingPoint.y, 'base');
  base.scale.x *= 1 * scale;
  base.scale.y *= 1 * scale;
  base.x = base.x - base.width * 0.5;
  base.y = base.y - base.height * 0.5;
  base.body.immovable = true;

  var indicator = hud.create(400, 300, 'base_indicator');
  indicator.drop = base;

  hudOxygen = game.add.text(0,0, 'Oxygen: 100.0', {fill:'white'}, hud);
  hudBattery = game.add.text(0,30, 'Battery: 100.0', {fill:'white'}, hud);
  hudBattery.visible = false;


  emitterPlayer = game.add.emitter(0, 0, 200);

  emitterPlayer.makeParticles('smoke');
  emitterPlayer.gravity = 0;

  emitterPlayer.minParticleScale = 0.1;
  emitterPlayer.maxParticleScale = 0.2;

  emitterPlayer.minParticleAlpha = 0.5;
  emitterPlayer.maxParticleAlpha = 1.0;

  emitterPlayer.maxParticleSpeed = 0.0;

  emitterPlayer.minRotation = 0;
  emitterPlayer.maxRotation = 200;

  emitterPlayer.particleSendToBack = false;


  emitterPlayer.start(false, 1000, 150);


  worm.scale.x *= scale;
  worm.scale.y *= scale;

  game.physics.arcade.enable(worm);

  playerShield = game.add.sprite(startingPoint.x, startingPoint.y, 'playerShield');
  playerShield.exists = false;

  var playerSprite = game.add.sprite(startingPoint.x, startingPoint.y, 'dude');
  playerSprite.animations.add('left', [0, 1, 2, 3], 10, true);
  playerSprite.animations.add('right', [5, 6, 7, 8], 10, true);
  playerSprite.animations.add('stand', [4], 1, true);
  playerSprite.animations.play('stand');

  playerSprite.scale.x *= scale;
  playerSprite.scale.y *= scale;

  game.physics.arcade.enable(playerSprite);
  playerSprite.body.bounce.x = 0.1;
  playerSprite.body.bounce.y = 0.1;
  playerSprite.body.collideWorldBounds = true;


  player = playerFactory.create(playerSprite);



  game.camera.follow(player.sprite);





  emitterVessel = game.add.emitter(startingPoint.x +40, startingPoint.y - 10, 200);

  emitterVessel.makeParticles('smokeBlack');
  emitterVessel.gravity = 0;

  emitterVessel.setXSpeed(15, 20);
  emitterVessel.setYSpeed(-5, -10);

  emitterVessel.minParticleScale = 0.5;
  emitterVessel.maxParticleScale = 1.0;

  emitterVessel.minParticleAlpha = 0.5;
  emitterVessel.maxParticleAlpha = 1.0;



  emitterVessel.minRotation = 0;
  emitterVessel.maxRotation = 200;

  emitterVessel.particleSendToBack = false;


  emitterVessel.start(false, 5000, 200);



  emitter = game.add.emitter(0, 0, 200);

  emitter.makeParticles('smoke');
  emitter.gravity = 0;

  emitter.minParticleScale = 0.5;
  emitter.maxParticleScale = 1.0;

  emitter.minParticleAlpha = 0.5;
  emitter.maxParticleAlpha = 1.0;

  emitter.maxParticleSpeed = 0.0;

  emitter.minRotation = 0;
  emitter.maxRotation = 200;

  emitter.particleSendToBack = false;


  emitter.start(false, 1000, 50);


  hintText = game.add.text(0,30, 'hint', {fill:'black'});
  hintText.visible = false;

}



var lastPressed = 0;
function doubleClick(pointer)
{
  var time = game.time.totalElapsedSeconds();

  if (time - lastPressed < 0.2)
    player.act(player.actions.dash, null);

  lastPressed = time;
}

function move()
{
  var target = new Phaser.Point(
    game.camera.x + game.input.x,
    game.camera.y + game.input.y
  );
  player.act(player.actions.move, {target: target});
}


function shock()
{
  player.act(player.actions.shock, null);
}

function updateHUDElement(element)
{
  if (!element.drop)
    return ;

  var hide = player.stats.loaded;

  element.exists = element.drop.exists && (!hide || element.drop.key == 'base');
  if (!element.exists)
    return ;

  var dist = Phaser.Point.distance(player.sprite, element.drop);
  element.scale.x = 0.3 + (1 - dist / (game.world.width * 0.015));
  element.scale.y = 0.3 + (1 - dist / (game.world.width * 0.015));

  var pointE = new Phaser.Point(element.cameraOffset.x, element.cameraOffset.y);
  var pointO = new Phaser.Point(400, 300);
  Phaser.Point.rotate(element.cameraOffset, 400, 300, game.physics.arcade.angleBetween(player.sprite, element.drop), false, 250);

  element.x = element.cameraOffset.x;
  element.y = element.cameraOffset.y;

}


function updateEmitterPlayer()
{
  emitterPlayer.on = player.sprite.body.velocity.x || player.sprite.body.velocity.y;
  emitterPlayer.x = player.sprite.body.center.x;
  emitterPlayer.y = player.sprite.body.center.y + player.sprite.height * 0.5;

  emitterWind.x = emitterPlayer.x;
  emitterWind.y = emitterPlayer.y;
}

var savedDir;
var charge = false;
var chargeNb = 0;
var immune = -1;

function updateWorm()
{
  var dir = new Phaser.Point(player.sprite.x - worm.x, player.sprite.y - worm.y);
  var dist = dir.getMagnitude();

  emitter.x = worm.body.center.x;
  emitter.y = worm.body.center.y;


  if (player.safe)
  {
    dir.setMagnitude(200 * 0.5);

    var factor = (dist / (player.sprite.width * 7)) - 1;

    worm.body.velocity.x = -dir.y * worm.scale.y + dir.x * factor;
    worm.body.velocity.y = dir.x * worm.scale.x + dir.y * factor;

    chargeNb = 0;
  }
  else
  {
    if (!charge && dist < player.sprite.width * 2)
    {
      charge = true;
      chargeNb += 1;

    }
    else if (dist > player.sprite.width * (6 - chargeNb))
      charge = false;

    if (charge && savedDir)
      dir = savedDir;
    else
      savedDir = dir;

    dir.setMagnitude(200 * 1.5 * (1 + chargeNb * 0.1));
    worm.body.velocity.x = dir.x * worm.scale.x;
    worm.body.velocity.y = dir.y * worm.scale.y;
  }

  if (worm.body.velocity.x > 0)
    worm.animations.play('right');
  else
    worm.animations.play('left');

  var hit = (!player.safe && dist < player.sprite.width);

  if (hit && player.power['shield'])
  {
    hit = false;
    immune = 1.0;
    player.power['shield'] = false;
  }

  if (hit || player.stats.oxygen <= 0)
    game.state.start(game.state.current);
}

function updateTexts()
{
  hudOxygen.text = 'Oxygen : ' + player.stats.oxygen.toFixed(1);

  if (player.stats.oxygen < 5.0)
    hudOxygen.fill= 'red';

  hudBattery.visible = player.power['shock'];
  if (!player.power['shock'])
    return ;

  hudBattery.text = 'Battery : ' + player.stats.battery.toFixed(1);

  if (player.stats.battery >= 100.0)
    hudBattery.fill= 'green';
  else
    hudBattery.fill= 'white';

}

function updateShield()
{
  playerShield.exists = player.power.shield;
  playerShield.x = player.sprite.body.x + player.sprite.width * 0.5;
  playerShield.y = player.sprite.body.y + player.sprite.height * 0.5 + 4 ;
  playerShield.scale.x = 1.7;
  playerShield.scale.y = 1.7;
  playerShield.anchor.setTo(0.5, 0.5);
  playerShield.rotation -= 0.1;

}

function CollectDrop(playerSprite, drop)
{
  if (player.act(player.actions.pick, {drop: drop.key}))
  {
    switch (drop.key)
    {
      case 'shield': displayHint('Absorb one hit.'); break;
      case 'dash': displayHint('Double click to dash. Pay attention to your oxygen!'); break;
      case 'shock': displayHint('Press space to repel the worm.');break;
    }
    drop.kill();
  }
}

function DropDrops(playerSprite, obstacle)
{
  if (obstacle.key == 'base')
    player.act(player.actions.drop, null)
}

function OnPlateform(playerSprite, platform)
{
  player.safe = true;
}

var wormSmoke = true;

function WormUnderPlateform()
{
  wormSmoke = false;
}

function smokeUpdate(particule)
{
  particule.alpha = particule.lifespan / 1000;
  particule.scale.x = 0.7 + 1 - particule.lifespan / 1000;
  particule.scale.y = 0.7 + 1 - particule.lifespan / 1000;
  if (particule.alpha < 0)
    particule.alpha = 0;
}


function smokeFootUpdate(particule)
{
  particule.alpha = particule.lifespan / 1000;
  particule.scale.x = 0.1 + 1 - particule.lifespan / 1000;
  particule.scale.y = 0.1 + 1 - particule.lifespan / 1000;
  if (particule.alpha < 0)
    particule.alpha = 0;
}


function smokeWindUpdate(particule)
{
  particule.alpha = particule.lifespan / 2000;
  particule.scale.x = 0.3 + 0.5 * (1 - particule.lifespan / 2000);
  particule.scale.y = 0.3 + 0.5 * (1 - particule.lifespan / 2000);
  if (particule.alpha < 0)
    particule.alpha = 0;
}

function smokeVesselUpdate(particule)
{
  particule.alpha = particule.lifespan / 5000;
  particule.scale.x = 0.1 + 2.0 * (1 - particule.lifespan / 5000);
  particule.scale.y = 0.1 + 2.0 * (1 - particule.lifespan / 5000);
  if (particule.alpha < 0)
    particule.alpha = 0;
}

var hint = null;
var time = 0;

function displayHint(text)
{
  hint = text;
  time = 5.0;
}

function updateHint()
{

  time -= game.time.physicsElapsed;

  if (hint)
    hintText.setText(hint);
  hintText.alpha = time / 5.0;

  if (hintText.alpha > 1.0)
    hintText.alpha = 1.0;
  if (hintText.alpha < 0.0)
    hintText.alpha = 0.0

  hintText.visible = true;
  hintText.anchor.setTo(0.5, 0.5);
  hintText.x = player.sprite.x + player.sprite.width * 0.5;
  hintText.y = player.sprite.y - 20;
}

function realUpdate()
{
  game.camera.update();

  updateEmitterPlayer();

  hud.forEach(updateHUDElement, this);
  emitter.forEachAlive(smokeUpdate, this);
  emitterPlayer.forEachAlive(smokeFootUpdate, this);
  emitterWind.forEachAlive(smokeWindUpdate, this);
  emitterVessel.forEachAlive(smokeVesselUpdate, this);

  if (immune > 0)
    immune -= game.time.physicsElapsed;

  player.safe = (immune > 0 || player.stats.shielded);
  wormSmoke = true;

  game.physics.arcade.collide(player.sprite, obstacles, DropDrops, null, this);
  game.physics.arcade.overlap(player.sprite, drops, CollectDrop, null, this);
  game.physics.arcade.overlap(player.sprite, platforms, OnPlateform, null, this);
  game.physics.arcade.overlap(worm, platforms, WormUnderPlateform, null, this);


  emitter.on = wormSmoke;

  game.input.onDown.add(doubleClick, this);
  if (game.input.mouse.button == Phaser.Mouse.LEFT_BUTTON)
    move();

  updateHint();

  updateShield();
  player.update(game.time.physicsElapsed);

  updateWorm();

  updateTexts();
}

function update()
{
  realUpdate();
}


var gameState = { preload: preload, create: create, update: update };

var preloadState = {
  preload: function () {
    this.load.image('preloaderBar', 'assets/preloaderBar.png');
    },

  create: function () {
    this.game.state.start('game');
    }
};


function Main(phaser, player)
{
  playerFactory = player;
  game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas', preloadState);
  game.state.add('game', gameState);
}


